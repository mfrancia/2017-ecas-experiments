# -*- coding: utf-8 -*-
"""
Created on Tue May  3 11:49:12 2016

@author: w4bo
"""

TIME            = 0 # - time
DEVICES         = 1 #  - number-of-nodes
STABLE          = 2 #  - molecule: stable
NEEDEDA         = 3 #  - molecule: neededA
NEEDEDB         = 4 #  - molecule: neededB
NEEDEDC         = 5 #  - molecule: neededC
NEEDEDD         = 6 #  - molecule: neededD
RESOURCES       = 7 #  - molecule: resources
ARRIVEDA        = 8 #  - molecule: countA
ARRIVEDB        = 9 #  - molecule: countB
ARRIVEDC        = 10 #  - molecule: countC
ARRIVEDD        = 11 #  - molecule: countD
DISTARRIVEDA    = 12 #  - molecule: distArrivedA
DISTARRIVEDB    = 13 #  - molecule: distArrivedB
DISTARRIVEDC    = 14 #  - molecule: distArrivedC
DISTARRIVEDD    = 15 #  - molecule: distArrivedD
DISTN           = 16 #  - molecule: distN
AVGRESARRIVED   = 17
RATIOA          = 18
RATIOB          = 19
RATIOC          = 20
RATIOD          = 21
AVGRATIO        = 22
AVGARRIVEDDIST  = 23

xmax=20

import numpy as np
import matplotlib.pyplot as plt

params = {'legend.fontsize': 14,
          'legend.loc': 'best',
          'axes.labelsize': 17,
          'axes.titlesize': 20,
          'xtick.labelsize': 17,
          'ytick.labelsize': 17,
          'axes.linewidth': 1.0,
          'font.size': 20,
          'font.family': 'sans-serif',
          'grid.color': 'k',
          'grid.linestyle': ':',
          'grid.linewidth': 0.5,
          'axes.xmargin': 0.0,
          'axes.ymargin': 0.1,
          'axes.axisbelow': False,
          'legend.frameon': False,
          'errorbar.capsize': 3
         }
plt.rcParams.update(params)

outputDirectory = "charts/res/"
#p = {"outName": "res", "title": "\"Resource Allocation\"", "name": ["_high", "_low", "_asymm"], "inputDir":"data/newresall/"}
p = {"outName": "res", "title": "\"Resource Allocation\"", "name": ["_asymm"], "inputDir":"data/newresall/"}

figsize=(7,3)
colors = ['#FFAA00', '#00FF00', '#00AAFF', '#AA00FF']
mapping = { 
    'flex': { 'short': 'L', 'extended': 'Library' }, 
    'naive': { 'short': 'BB', 'extended': 'BB-Only'  }, 
    '_low': { 'short': 'Low', 'extended': 'Low - '},
    '_high': { 'short': 'High', 'extended': 'High - '}, 
    '_asymm': { 'short': 'Asym', 'extended': 'Asymmetric - '},
}
flag = True
for n in p['name']:
    print(n)
    inputDirectory = p['inputDir']
    fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=figsize)
    fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=figsize)
    for n2 in ['flex', 'naive']:
        avg = np.genfromtxt(inputDirectory + n2 + n + "-mean_freq-0.5.txt", dtype=float, delimiter=' ')
        std = np.genfromtxt(inputDirectory + n2 + n + "-err_freq-0.5.txt", dtype=float, delimiter=' ') 
        X               = np.array([x[TIME] for x in avg])/60
        resources       = np.array([x[RESOURCES] for x in avg])
        stable          = np.array([x[RESOURCES] - x[STABLE] for x in avg])
        stablestd       = np.array([x[STABLE] for x in std])
        arriveda        = np.array([x[ARRIVEDA] for x in avg]) 
        arrivedastd     = np.array([x[ARRIVEDA] for x in std])
        arrivedb        = np.array([x[ARRIVEDB] for x in avg]) 
        arrivedbstd     = np.array([x[ARRIVEDB] for x in std])    
        arrivedc        = np.array([x[ARRIVEDC] for x in avg]) 
        arrivedcstd     = np.array([x[ARRIVEDC] for x in std])
        arrivedd        = np.array([x[ARRIVEDD] for x in avg]) 
        arriveddstd     = np.array([x[ARRIVEDD] for x in std])

        style = '-'
        marker = 's'
        c = 'blue'
        if n2.startswith('naive'):
            style = '--'
            marker = 'o'
            c = 'green'
        #if n2.startswith('flex'):   
        #    ax1.plot(X, resources, color=colors[0], label="Resources", linewidth=3)
        ax1.plot(X, stable, color=c, label=mapping[n2]['short'], linestyle = style, linewidth=3)
        ax1.plot(X, stable - stablestd, color=c, linestyle = style, linewidth=0.5)
        ax1.plot(X, stable + stablestd, color=c, linestyle = style, linewidth=0.5)
        
        ax2.plot(X, arriveda, color=colors[0], linestyle = style, label = mapping[n2]['short'] + "-A", linewidth=3)
        ax2.plot(X, arriveda + arrivedastd, color=colors[0], linestyle = style, linewidth=0.5)
        ax2.plot(X, arriveda - arrivedastd, color=colors[0], linestyle = style, linewidth=0.5)
        ax2.plot(X, arrivedb, color=colors[1], linestyle = style, label = mapping[n2]['short'] + "-B", linewidth=3)
        ax2.plot(X, arrivedb + arrivedbstd, color=colors[1], linestyle = style, linewidth=0.5)
        ax2.plot(X, arrivedb - arrivedbstd, color=colors[1], linestyle = style, linewidth=0.5) 
        ax2.plot(X, arrivedc, color=colors[2], linestyle = style, label = mapping[n2]['short'] + "-C", linewidth=3)
        ax2.plot(X, arrivedc + arrivedcstd, color=colors[2], linestyle = style, linewidth=0.5)
        ax2.plot(X, arrivedc - arrivedcstd, color=colors[2], linestyle = style, linewidth=0.5)
        ax2.plot(X, arrivedd, color=colors[3], linestyle = style, label = mapping[n2]['short'] + "-D", linewidth=3)
        ax2.plot(X, arrivedd + arriveddstd, color=colors[3], linestyle = style, linewidth=0.5)
        ax2.plot(X, arrivedd - arriveddstd, color=colors[3], linestyle = style, linewidth=0.5)

    ax1.set_xlim(xmin=0, xmax=xmax)
    ax1.set_ylim(ymin=13, ymax=15.5)
    ax1.set_title(mapping[n]['extended'] + ' stabilization')
    ax1.set_ylabel("Resources")
    ax1.set_xlabel("Simulation time (mins)")
    #ax1.legend(ncol=4)
    ax2.set_title(mapping[n]['extended'] + ' resources serving needs')
    ax2.set_ylabel("Resources")
    ax2.set_xlabel("Simulation time (mins)")
    #if n.endswith('_high') or n.endswith('_asymm'):
    #    ax2.set_ylim(ymin=0, ymax = 7.5)
    #else:
    #    ax2.set_ylim(ymin=0, ymax = 5.5)
    ax2.set_ylim(ymin=0)
    ax2.set_xlim(xmin=0, xmax=xmax)
    #ax2.legend(ncol=1)
    box = ax2.get_position()
    ax2.set_position([box.x0, box.y0, box.width * 0.7, box.height])
    ax2.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=1)
    
    fig1.tight_layout()
    fig1.show() 
    fig2.tight_layout()
    fig2.show() 

    if flag:
        handles,labels = ax1.get_legend_handles_labels()
        fig, axe= plt.subplots(nrows=1, ncols=1, figsize=(0.01,0.01))
        axe.axis('off')
        axe.legend(handles, labels)
        fig.savefig(outputDirectory + p['outName'] + '_legend_stable.pdf',bbox_inches='tight')
        handles,labels = ax2.get_legend_handles_labels()
        fig, axe= plt.subplots(nrows=1, ncols=1, figsize=(0.01,0.01))
        axe.axis('off')
        axe.legend(handles, labels)
        fig.savefig(outputDirectory + p['outName'] + '_legend_needserved.pdf',bbox_inches='tight')
        
    fig1.savefig(outputDirectory + p['outName'] + n +'_1stable.pdf',bbox_inches='tight')
    fig2.savefig(outputDirectory + p['outName'] + n +'_2needserved.pdf',bbox_inches='tight')