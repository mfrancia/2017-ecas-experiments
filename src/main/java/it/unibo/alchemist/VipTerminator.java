package it.unibo.alchemist;

import it.unibo.alchemist.boundary.interfaces.OutputMonitor;
import it.unibo.alchemist.model.implementations.nodes.ProtelisNode;
import it.unibo.alchemist.model.implementations.times.DoubleTime;
import it.unibo.alchemist.model.interfaces.Environment;
import it.unibo.alchemist.model.interfaces.Reaction;
import it.unibo.alchemist.model.interfaces.Time;

public class VipTerminator<T> implements OutputMonitor<T>{
    private static final long serialVersionUID = 1L;
    private Time time;
    public VipTerminator() { time = new DoubleTime(0); }

    @Override
    public void finished(Environment<T> env, Time time, long step) {
    }

    @Override
    public void initialized(Environment<T> env) {
    }

    @Override
    public void stepDone(Environment<T> env, Reaction<T> r, Time time, long step) {
        if (time.toDouble() > 600 && time.toDouble() - this.time.toDouble() > 10) {
            final double allArrived = 
                            env.getNodes()
                                .stream()
                                .filter(n -> { return ((ProtelisNode) n).has("arrivedA") || ((ProtelisNode) n).has("arrivedB") || ((ProtelisNode) n).has("arrivedC"); })
                                .map(n -> { return (double) ((ProtelisNode) n).get("arrivedA") + (double) ((ProtelisNode) n).get("arrivedB") + (double) ((ProtelisNode) n).get("arrivedC"); } )
                                .reduce(0.0, (a, b) -> { return a + b; });
            final double allCount = 
                        env.getNodes()
                        .stream()
                        .filter(n -> { return ((ProtelisNode) n).has("countA") || ((ProtelisNode) n).has("countB") || ((ProtelisNode) n).has("countC"); })
                        .map(n -> { return (double) ((ProtelisNode) n).get("countA") + (double) ((ProtelisNode) n).get("countB") + (double) ((ProtelisNode) n).get("countC"); } )
                        .reduce(0.0, (a, b) -> { return a + b; });
            if (allCount > 0 && allArrived > 0 && (allArrived == allCount)) { 
                env.getSimulation().terminate(); 
                System.out.println(time.toDouble() + ": Vip Done.");
            }
            this.time = time;
        }
    }

}
