# -*- coding: utf-8 -*-
"""
Created on Tue May  3 11:49:12 2016

@author: w4bo
"""

TIME            = 0 # - time
DEVICES         = 1 #  - number-of-nodes
STABLE          = 2 #  - molecule: stable
NEEDEDA         = 3 #  - molecule: neededA
NEEDEDB         = 4 #  - molecule: neededB
NEEDEDC         = 5 #  - molecule: neededC
NEEDEDD         = 6 #  - molecule: neededD
RESOURCES       = 7 #  - molecule: resources
ARRIVEDA        = 8 #  - molecule: countA
ARRIVEDB        = 9 #  - molecule: countB
ARRIVEDC        = 10 #  - molecule: countC
ARRIVEDD        = 11 #  - molecule: countD
DISTARRIVEDA    = 12 #  - molecule: distArrivedA
DISTARRIVEDB    = 13 #  - molecule: distArrivedB
DISTARRIVEDC    = 14 #  - molecule: distArrivedC
DISTARRIVEDD    = 15 #  - molecule: distArrivedD
DISTN           = 16 #  - molecule: distN
AVGRESARRIVED   = 17
RATIOA          = 18
RATIOB          = 19
RATIOC          = 20
RATIOD          = 21
AVGRATIO        = 22
AVGARRIVEDDIST  = 23

import numpy as np
import fnmatch
import os
import re

def getDistServed(arrived, needed, dist, m):
    tmp = np.where(np.nan_to_num(m[:,arrived]) > m[:,needed])[0]
    print(tmp)
    if (tmp.size > 0):
        return m[tmp[0]-1, dist]
    else:
        return m[-1, dist] 

def convFloat(x, limit='inf'):
    try:
        result = float(x)
        if result == float('inf') or result == float('-inf') or result == float('NaN') or result > limit:
            return float('0')
        return result
    except ValueError:
        return float('NaN')

def lineToArray(line):
    return [convFloat(x, limit = 10e6) for x in line.split()]

def openCsv(path):
    with open(path, 'r') as file:
        lines = filter(lambda x: re.compile('\d').match(x[0]), file.readlines())
        return [lineToArray(line) for line in lines]

rootdirectory = 'data/newresall_raw'
outputdir = 'data/newresall_raw'
subDirectories = ['flexGradient', 'naive']
floatPrecision = '{: 0.2f}'
np.set_printoptions(formatter={'float': floatPrecision.format})
    
for directory in subDirectories:
    path = rootdirectory + '/' + directory
    allFiles = list(filter(lambda file: fnmatch.fnmatch(file, '*.txt'), os.listdir(path)))
    for file in allFiles:
        print(file)
        contents = np.array(openCsv(path + '/' + file))
        contents[np.isnan(contents)]=0

        distServedA = getDistServed(ARRIVEDA, NEEDEDA, DISTARRIVEDA, contents)
        distServedB = getDistServed(ARRIVEDB, NEEDEDB, DISTARRIVEDB, contents)
        distServedC = getDistServed(ARRIVEDC, NEEDEDC, DISTARRIVEDC, contents)
        distServedD = getDistServed(ARRIVEDD, NEEDEDD, DISTARRIVEDD, contents)

        wastedA = np.array([x[DISTARRIVEDA] - distServedA if x[ARRIVEDA] > x[NEEDEDA] else 0 for x in contents])
        wastedB = np.array([x[DISTARRIVEDB] - distServedB if x[ARRIVEDB] > x[NEEDEDB] else 0 for x in contents])
        wastedC = np.array([x[DISTARRIVEDC] - distServedC if x[ARRIVEDC] > x[NEEDEDC] else 0 for x in contents])
        wastedD = np.array([x[DISTARRIVEDD] - distServedD if x[ARRIVEDD] > x[NEEDEDD] else 0 for x in contents])

        contents[:, DISTARRIVEDA] = np.array([x[DISTARRIVEDA] if x[ARRIVEDA] <= x[NEEDEDA] else distServedA for x in contents])
        contents[:, DISTARRIVEDB] = np.array([x[DISTARRIVEDB] if x[ARRIVEDB] <= x[NEEDEDB] else distServedB for x in contents])
        contents[:, DISTARRIVEDC] = np.array([x[DISTARRIVEDC] if x[ARRIVEDC] <= x[NEEDEDC] else distServedC for x in contents])
        contents[:, DISTARRIVEDD] = np.array([x[DISTARRIVEDD] if x[ARRIVEDD] <= x[NEEDEDD] else distServedD for x in contents])
        contents[:, DISTN] = contents[:, DISTN] + wastedA + wastedB + wastedC + wastedD

        avgResArrived = (contents[:,ARRIVEDA] + contents[:,ARRIVEDB] + contents[:,ARRIVEDC] + contents[:,ARRIVEDD])/4
        ratioA = contents[:,ARRIVEDA]/np.maximum(contents[:,NEEDEDA], 1)
        ratioB = contents[:,ARRIVEDB]/np.maximum(contents[:,NEEDEDB], 1)
        ratioC = contents[:,ARRIVEDC]/np.maximum(contents[:,NEEDEDC], 1)
        ratioD = contents[:,ARRIVEDD]/np.maximum(contents[:,NEEDEDD], 1)
        avgRatio = (ratioA + ratioB + ratioC + ratioD)/4
        avgArrivedDistance = (contents[:,DISTARRIVEDA] / np.maximum(contents[:,ARRIVEDA], 1) + 
                              contents[:,DISTARRIVEDB] / np.maximum(contents[:,ARRIVEDB], 1) + 
                              contents[:,DISTARRIVEDC] / np.maximum(contents[:,ARRIVEDC], 1) + 
                              contents[:,DISTARRIVEDD] / np.maximum(contents[:,ARRIVEDD], 1)) / 4

        c = np.c_[contents, avgResArrived, ratioA, ratioB, ratioC, ratioD, avgRatio, avgArrivedDistance]
        np.savetxt(outputdir + '/' + directory + 'Extended/' + file, c)