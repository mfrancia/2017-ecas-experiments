# -*- coding: utf-8 -*-
"""
Created on Tue May  3 11:49:12 2016

@author: w4bo
"""

TIME = 0      # - time
NODES = 1     # - number-of-nodes
STABLE = 2    # - molecule: stable
ISGOING = 3   # - molecule: isGoing
CROWDTHR = 4  # - molecule: crowdThr
COUNTA = 5    # - molecule: countA
COUNTB = 6    # - molecule: countB
COUNTC = 7    # - molecule: countC
ARRIVEDA = 8  # - molecule: arrivedA
ARRIVEDB = 9  # - molecule: arrivedB
ARRIVEDC = 10 # - molecule: arrivedC
DISTA = 11    # - molecule: distA
DISTB = 12    # - molecule: distB
DISTC = 13    # - molecule: distC
DISTN = 14    # - molecule: distN
DISTARRIVEDA = 15    # - molecule: distArrivedA
DISTARRIVEDB = 16    # - molecule: distArrivedB
DISTARRIVEDC = 17    # - molecule: distArrivedC
SERVEDA = 18
SERVEDB = 19
SERVEDC = 20
SUMSERVED = 21
AVERAGESERVED = 22
RATIOSERVED = 23
NOTSERVEDA = 24
NOTSERVEDB = 25
NOTSERVEDC = 26
SUMNOTSERVED = 27
AVERAGENOTSERVED = 28
DISTSERVEDA = 29 
DISTSERVEDB = 30 
DISTSERVEDC = 31  
AVGDISTSERVEDA = 32
AVGDISTSERVEDB = 33 
AVGDISTSERVEDC = 34
AVGDISTSERVED  = 35
DISTNOTSERVEDA = 36    
DISTNOTSERVEDB = 37    
DISTNOTSERVEDC = 38
SUMDISTNOTSERVED = 39
SUMDISTWASTED = 40
AVGDISTNOTSERVEDA = 41
AVGDISTNOTSERVEDB = 42
AVGDISTNOTSERVEDC = 43

import numpy as np
import fnmatch
import os
import re

def convFloat(x, limit='inf'):
    try:
        result = float(x)
        if result == float('inf') or result == float('-inf') or result == float('NaN') or result > limit:
            return float('0')
        return result
    except ValueError:
        return float('NaN')

def lineToArray(line):
    return [convFloat(x, limit = 10e6) for x in line.split()]

def openCsv(path):
    with open(path, 'r') as file:
        lines = filter(lambda x: re.compile('\d').match(x[0]), file.readlines())
        return [lineToArray(line) for line in lines]

rootdirectory = 'data/newvip_raw'
outputdir = 'data/newvip_raw2'
subDirectories = ['flexGradient', 'naive']
#subDirectories = ['naive']
floatPrecision = '{: 0.2f}'
np.set_printoptions(formatter={'float': floatPrecision.format})

def getDistServed(arrived, dist, m):
    tmp = np.where(np.nan_to_num(m[:,arrived]) >= m[:,CROWDTHR])[0]
    if (tmp.size > 0):
        return m[tmp[0], dist]
    else:
        return m[-1, dist] 
    
for directory in subDirectories:
    path = rootdirectory + '/' + directory
    allFiles = list(filter(lambda file: fnmatch.fnmatch(file, '*.txt'), os.listdir(path)))
    for file in allFiles:
        print(file)
        contents = np.array(openCsv(path + '/' + file))
        contents[np.isnan(contents)]=0
        
        thr = contents[:,CROWDTHR]
        
        _distServedA = getDistServed(ARRIVEDA, DISTARRIVEDA, contents)
        _distServedB = getDistServed(ARRIVEDB, DISTARRIVEDB, contents)
        _distServedC = getDistServed(ARRIVEDC, DISTARRIVEDC, contents)
        
        _ARRIVEDA = contents[:,ARRIVEDA]
        _ARRIVEDB = contents[:,ARRIVEDB]
        _ARRIVEDC = contents[:,ARRIVEDC]
        _ISGOING = contents[:,ISGOING]
        
        servedA = np.minimum(_ARRIVEDA, thr)
        servedB = np.minimum(_ARRIVEDB, thr)
        servedC = np.minimum(_ARRIVEDC, thr)
        sumServed = servedA + servedB + servedC
        averageServed = sumServed / 3
        tmp = np.minimum(thr * 3, _ISGOING)
        ratioServed = sumServed / np.maximum(tmp, 1) #np.divide(sumServed, np.maximum(tmp, 1))
        notServedA = np.maximum(_ARRIVEDA - thr, 0) 
        notServedB = np.maximum(_ARRIVEDB - thr, 0) 
        notServedC = np.maximum(_ARRIVEDC - thr, 0)
        sumNotServed = notServedA + notServedB + notServedC
        averageNotServed = sumNotServed / 3
        distServedA = np.array([x[DISTARRIVEDA] if x[ARRIVEDA] <= x[CROWDTHR] else _distServedA for x in contents])
        distServedB = np.array([x[DISTARRIVEDB] if x[ARRIVEDB] <= x[CROWDTHR] else _distServedB for x in contents])
        distServedC = np.array([x[DISTARRIVEDC] if x[ARRIVEDC] <= x[CROWDTHR] else _distServedC for x in contents])
        avgDistServedA = distServedA / np.maximum(servedA, 1) #np.divide(distServedA, np.maximum(servedA, 1))
        avgDistServedB = distServedB / np.maximum(servedB, 1) #np.divide(distServedB, np.maximum(servedB, 1))
        avgDistServedC = distServedC / np.maximum(servedC, 1) #np.divide(distServedC, np.maximum(servedC, 1))
        avgDistServed = (avgDistServedA + avgDistServedB + avgDistServedC) / 3
        distNotServedA = np.array([x[DISTARRIVEDA] - _distServedA if x[ARRIVEDA] > x[CROWDTHR] else 0 for x in contents])
        distNotServedB = np.array([x[DISTARRIVEDB] - _distServedB if x[ARRIVEDB] > x[CROWDTHR] else 0 for x in contents])
        distNotServedC = np.array([x[DISTARRIVEDC] - _distServedC if x[ARRIVEDC] > x[CROWDTHR] else 0 for x in contents])
        sumDistNotServed = distNotServedA + distNotServedB + distNotServedC
        sumDistWasted = sumDistNotServed + contents[:, DISTN]
        avgDistNotServedA = distNotServedA / np.maximum(notServedA, 1) #np.divide(distNotServedA, np.maximum(notServedA, 1))
        avgDistNotServedB = distNotServedB / np.maximum(notServedB, 1) #np.divide(distNotServedB, np.maximum(notServedB, 1))
        avgDistNotServedC = distNotServedC / np.maximum(notServedC, 1) #np.divide(distNotServedC, np.maximum(notServedC, 1))
        c = np.c_[contents, 
                    servedA, servedB, servedC, sumServed, averageServed, ratioServed,
                    notServedA, notServedB, notServedC, sumNotServed, averageNotServed,
                    distServedA, distServedB, distServedC, avgDistServedA, avgDistServedB, avgDistServedC, avgDistServed,
                    distNotServedA, distNotServedB, distNotServedC, sumDistNotServed, sumDistWasted,
                    avgDistNotServedA, avgDistNotServedB, avgDistNotServedC]
        np.savetxt(outputdir + '/' + directory + '/' + file, c)