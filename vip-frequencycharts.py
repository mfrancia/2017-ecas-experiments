# -*- coding: utf-8 -*-
"""
Created on Tue May  3 11:49:12 2016

@author: w4bo
"""

TIME = 0      # - time
NODES = 1     # - number-of-nodes
STABLE = 2    # - molecule: stable
ISGOING = 3   # - molecule: isGoing
CROWDTHR = 4  # - molecule: crowdThr
COUNTA = 5    # - molecule: countA
COUNTB = 6    # - molecule: countB
COUNTC = 7    # - molecule: countC
ARRIVEDA = 8  # - molecule: arrivedA
ARRIVEDB = 9  # - molecule: arrivedB
ARRIVEDC = 10 # - molecule: arrivedC
DISTA = 11    # - molecule: distA
DISTB = 12    # - molecule: distB
DISTC = 13    # - molecule: distC
DISTN = 14    # - molecule: distN
DISTARRIVEDA = 15    # - molecule: distArrivedA
DISTARRIVEDB = 16    # - molecule: distArrivedB
DISTARRIVEDC = 17    # - molecule: distArrivedC
SERVEDA = 18
SERVEDB = 19
SERVEDC = 20
SUMSERVED = 21
AVERAGESERVED = 22
RATIOSERVED = 23
NOTSERVEDA = 24
NOTSERVEDB = 25
NOTSERVEDC = 26
SUMNOTSERVED = 27
AVERAGENOTSERVED = 28
DISTSERVEDA = 29 
DISTSERVEDB = 30 
DISTSERVEDC = 31  
AVGDISTSERVEDA = 32
AVGDISTSERVEDB = 33 
AVGDISTSERVEDC = 34
AVGDISTSERVED  = 35
DISTNOTSERVEDA = 36    
DISTNOTSERVEDB = 37    
DISTNOTSERVEDC = 38
SUMDISTNOTSERVED = 39
SUMDISTWASTED = 40
AVGDISTNOTSERVEDA = 41
AVGDISTNOTSERVEDB = 42
AVGDISTNOTSERVEDC = 43  

import numpy as np
import matplotlib.pyplot as plt
import fnmatch
import os
import re

outputDirectory = "charts/vip/"
files = ['flex_high', 'flex_medium', 'flex_low', 'naive_high', 'naive_medium', 'naive_low']
path = 'data/newvip2/'
var = 'freq'

params = {'legend.fontsize': 14,
          'legend.loc': 'best',
          'axes.labelsize': 17,
          'axes.titlesize': 20,
          'xtick.labelsize': 17,
          'ytick.labelsize': 17,
          'axes.linewidth': 1.0,
          'font.size': 20,
          'font.family': 'sans-serif',
          'grid.color': 'k',
          'grid.linestyle': ':',
          'grid.linewidth': 0.5,
          'axes.xmargin': 0.0,
          'axes.ymargin': 0.1,
          'axes.axisbelow': False,
          'legend.frameon': False,
          'errorbar.capsize': 3
         }
plt.rcParams.update(params)

colors = ['orange', 'green', 'blue', 'red', 'purple', 'black']
mapping = { 
    'flex': { 'short': 'L', 'extended': 'Library' }, 
    'naive': { 'short': 'BB', 'extended': 'BB-Only'  }, 
    '_low': { 'short': 'Low', 'extended': 'Low - '},
    '_medium': { 'short': 'High', 'extended': 'High - '}, 
    '_high': { 'short': 'Asym', 'extended': 'Asymmetric - '},
}

floatPrecision = '{: 0.2f}'
np.set_printoptions(formatter={'float': floatPrecision.format})

def convFloat(x, limit='inf'):
    try:
        result = float(x)
        if result == float('inf') or result == float('-inf') or result > limit:
            return float('NaN')
        return result
    except ValueError:
        return float('NaN')

def lineToArray(line):
    return [convFloat(x, limit = 10e6) for x in line.split()]

def openCsv(path):
    with open(path, 'r') as file:
        lines = filter(lambda x: re.compile('\d').match(x[0]), file.readlines())
        return [lineToArray(line) for line in lines]

def getVarValue(var, target):
    match = re.search('(?<=' + var +'-)\d+(\.\d*)?', target)
    return str(match.group(0))
                   
def getVector(f, name):
    allfiles = list(filter(lambda file: fnmatch.fnmatch(file, f + '-' + name + '_' + var + '-*.txt'), os.listdir(path)))
    floatre = '[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?'
    split = list(set(tuple(re.split('_'+var+'-'+floatre, x)) for x in allfiles))
    for descriptor in split:
        print(descriptor)
        unixMatch = descriptor[0] + '*' + descriptor[2]
        matchingFiles = filter(lambda f: fnmatch.fnmatch(f, unixMatch), os.listdir(path))
        contents = [] 
        for file in matchingFiles:
            c = openCsv(path + '/' + file)
            contents.append(sum([[float(getVarValue(var, file))], c[-1]], []))
        l = np.array(contents)
        #l = np.sort(np.array(contents), axis=0)
        return l

figsize=(7,3)
fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=figsize)
fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=figsize)
fig3, ax3 = plt.subplots(nrows=1, ncols=1, figsize=figsize)
fig4, ax4 = plt.subplots(nrows=1, ncols=1, figsize=figsize)
fig5, ax5 = plt.subplots(nrows=1, ncols=1, figsize=figsize)
fig6, ax6 = plt.subplots(nrows=1, ncols=1, figsize=figsize)

ax1.set_title("Avg served people (arrived before thr)")
ax1.set_ylabel("Devices")
ax1.set_xlabel("Computational round interval (s)")
ax2.set_title("Ratio of served people")
ax2.set_ylabel("Percentage")
ax2.set_xlabel("Computational round interval (s)")
ax3.set_title("Avgerage distance - served")
ax3.set_xlabel("Computational round interval (s)")
ax3.set_ylabel("Distance (km)")
ax4.set_title("Sum distances - wasted movement")
ax4.set_xlabel("Computational round interval (s)")
ax4.set_ylabel("Distance (km)")
ax5.set_ylabel("Sum of served people")
ax5.set_xlabel("Computational round interval (s)")
ax6.set_ylabel("Sum of NOT served people")
ax6.set_xlabel("Computational round interval (s)")

for f in files:

    mean = getVector(f, 'mean')
    std = getVector(f, 'err')

    X                   = 1/mean[:,0]
    servedA             = mean[:,SERVEDA + 1]
    servedAstd          = std[:,SERVEDA + 1]
    servedB             = mean[:,SERVEDB + 1]
    servedBstd          = std[:,SERVEDB + 1]
    servedC             = mean[:,SERVEDC + 1]
    servedCstd          = std[:,SERVEDC + 1]
    sumServed           = mean[:,SUMSERVED + 1]
    sumNotServed        = mean[:,SUMNOTSERVED + 1]
    avgServed           = mean[:,AVERAGESERVED + 1]
    avgDistServed       = mean[:,AVGDISTSERVED + 1] / 1000
    sumDistWasted       = mean[:,SUMDISTWASTED + 1] / 1000
    crowdthr            = mean[:,CROWDTHR + 1]
    notServedA          = mean[:,NOTSERVEDA + 1]
    notServedB          = mean[:,NOTSERVEDB + 1]
    notServedC          = mean[:,NOTSERVEDC + 1]
    ratio               = mean[:,RATIOSERVED + 1]
    sumServedstd        = std[:,SUMSERVED + 1]
    sumNotServedstd     = std[:,SUMNOTSERVED + 1]
    avgServedstd        = std[:,AVERAGESERVED + 1]
    avgDistServedstd    = std[:,AVGDISTSERVED + 1] / 1000 
    sumDistWastedstd    = std[:,SUMDISTWASTED + 1] / 1000
    crowdthrstd         = std[:,CROWDTHR + 1]
    notServedAstd       = std[:,NOTSERVEDA + 1]
    notServedBstd       = std[:,NOTSERVEDB + 1]
    notServedCstd       = std[:,NOTSERVEDC + 1]
    ratiostd            = std[:,RATIOSERVED + 1]   
    
    style = '-'
    marker = 's'
    color = 'black'
    if f.startswith('naive'):
        style = '--'
        marker = 'o'
    if f.endswith('high'):
        c = 'blue'  
    if f.endswith('medium'):
        c = 'orange'
    if f.endswith('low'):
        c = 'green'
 
    name = f.split("_")
    name = mapping[name[0]]['short']+'-'+mapping['_'+name[1]]['short']
    ax1.errorbar(X, avgServed, avgServedstd, color=c, linestyle = style, marker=marker, label=name, linewidth=1)
    
    if (f.startswith('flex') and f.endswith('high')):
        ax1.plot(X, crowdthr, color='red', linewidth=3)
        ax5.plot(X, crowdthr * 3, color='red', linewidth=3)
    
    
    ax2.errorbar(X, ratio, ratiostd, color=c, linestyle=style, marker=marker, label=name, linewidth=1)
    
    ax3.errorbar(X, avgDistServed, avgDistServedstd, color=c, linestyle=style, marker=marker, label=name, linewidth=1)
    
    ax4.errorbar(X, sumDistWasted, sumDistWastedstd, color=c, linestyle = style, marker=marker, label=name, linewidth=1)
    
    ax5.errorbar(X, sumServed, sumServedstd, color=c, linestyle=style, marker=marker, label=name, linewidth=1)
    
    ax6.errorbar(X, sumNotServed, sumNotServedstd, color=c, linestyle=style, marker=marker, label=name, linewidth=1)

    
ax1.set_xscale('log')
ax1.set_ylim(ymin=0)
#ax1.legend(ncol=3)
ax2.set_xscale('log')
ax2.set_ylim(ymin=0)
box = ax2.get_position()
ax2.set_position([box.x0, box.y0, box.width * 0.7, box.height])
ax2.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=1)
#ax2.legend(ncol=3)
ax3.set_xscale('log')
ax3.set_ylim(ymin=0)
#ax3.legend(ncol=3)
box = ax3.get_position()
ax3.set_position([box.x0, box.y0, box.width * 0.7, box.height])
ax3.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=1)
ax4.set_xscale('log')
ax4.set_ylim(ymin=0, ymax=150)
#ax4.legend(ncol=3)
box = ax4.get_position()
ax4.set_position([box.x0, box.y0, box.width * 0.7, box.height])
ax4.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=1)
ax5.set_xscale('log')
ax5.set_ylim(ymin=0)
#ax5.legend(ncol=3)
ax6.set_xscale('log')
ax6.set_ylim(ymin=0)
#ax6.legend(ncol=3) 

fig1.tight_layout()
fig1.show()
fig2.tight_layout()
fig2.show()
fig3.tight_layout()
fig3.show()
fig4.tight_layout()
fig4.show()
fig5.tight_layout()
fig5.show()
fig6.tight_layout()
fig6.show()

handles,labels = ax1.get_legend_handles_labels()
fig, axe= plt.subplots(nrows=1, ncols=1, figsize=(0.01,0.01))
axe.axis('off')
axe.legend(handles, labels)
fig.savefig(outputDirectory + 'vip_legend_freq.pdf',bbox_inches='tight')

#fig1.savefig(outputDirectory + 'vip_frequency_1.pdf')
fig2.savefig(outputDirectory + 'vip_frequency_2ratio.pdf',bbox_inches='tight')
fig3.savefig(outputDirectory + 'vip_frequency_3avgdist.pdf',bbox_inches='tight')
fig4.savefig(outputDirectory + 'vip_frequency_4sumdist.pdf',bbox_inches='tight')
#fig5.savefig(outputDirectory + 'vip_frequency_5.pdf')
#fig6.savefig(outputDirectory + 'vip_frequency_6.pdf')