# -*- coding: utf-8 -*-
"""
Created on Tue May  3 11:49:12 2016

@author: w4bo
"""

TIME            = 0 # - time
DEVICES         = 1 #  - number-of-nodes
STABLE          = 2 #  - molecule: stable
NEEDEDA         = 3 #  - molecule: neededA
NEEDEDB         = 4 #  - molecule: neededB
NEEDEDC         = 5 #  - molecule: neededC
NEEDEDD         = 6 #  - molecule: neededD
RESOURCES       = 7 #  - molecule: resources
ARRIVEDA        = 8 #  - molecule: countA
ARRIVEDB        = 9 #  - molecule: countB
ARRIVEDC        = 10 #  - molecule: countC
ARRIVEDD        = 11 #  - molecule: countD
DISTARRIVEDA    = 12 #  - molecule: distArrivedA
DISTARRIVEDB    = 13 #  - molecule: distArrivedB
DISTARRIVEDC    = 14 #  - molecule: distArrivedC
DISTARRIVEDD    = 15 #  - molecule: distArrivedD
DISTN           = 16 #  - molecule: distN
AVGRESARRIVED   = 17
RATIOA          = 18
RATIOB          = 19
RATIOC          = 20
RATIOD          = 21
AVGRATIO        = 22
AVGARRIVEDDIST  = 23

import numpy as np
import matplotlib.pyplot as plt
import fnmatch
import os
import re

outputDirectory = "charts/res/"
files = ['flex_asymm', 'flex_high', 'flex_low', 'naive_asymm', 'naive_high', 'naive_low']
path = 'data/newresall/'
var = 'freq'

params = {'legend.fontsize': 14,
          'legend.loc': 'best',
          'axes.labelsize': 17,
          'axes.titlesize': 20,
          'xtick.labelsize': 17,
          'ytick.labelsize': 17,
          'axes.linewidth': 1.0,
          'font.size': 20,
          'font.family': 'sans-serif',
          'grid.color': 'k',
          'grid.linestyle': ':',
          'grid.linewidth': 0.5,
          'axes.xmargin': 0.0,
          'axes.ymargin': 0.1,
          'axes.axisbelow': False,
          'legend.frameon': False,
          'errorbar.capsize': 3
         }
plt.rcParams.update(params)


colors = ['orange', 'green', 'blue', 'red', 'purple', 'black']
floatPrecision = '{: 0.2f}'
np.set_printoptions(formatter={'float': floatPrecision.format})
mapping = { 
    'flex': { 'short': 'L', 'extended': 'Library' }, 
    'naive': { 'short': 'BB', 'extended': 'BB-Only'  }, 
    '_low': { 'short': 'Low', 'extended': 'Low - '},
    '_high': { 'short': 'High', 'extended': 'High - '}, 
    '_asymm': { 'short': 'Asym', 'extended': 'Asymmetric - '},
}
def convFloat(x, limit='inf'):
    try:
        result = float(x)
        if result == float('inf') or result == float('-inf') or result > limit:
            return float('NaN')
        return result
    except ValueError:
        return float('NaN')

def lineToArray(line):
    return [convFloat(x, limit = 10e6) for x in line.split()]

def openCsv(path):
    with open(path, 'r') as file:
        lines = filter(lambda x: re.compile('\d').match(x[0]), file.readlines())
        return [lineToArray(line) for line in lines]

def getVarValue(var, target):
    match = re.search('(?<=' + var +'-)\d+(\.\d*)?', target)
    #print(match.group(0))
    return str(match.group(0))
                   
def getVector(f, name):
    allfiles = list(filter(lambda file: fnmatch.fnmatch(file, f + '-' + name + '_' + var + '-*.txt'), os.listdir(path)))
    floatre = '[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?'
    split = list(set(tuple(re.split('_'+var+'-'+floatre, x)) for x in allfiles))
    for descriptor in split:
        print(descriptor)
        unixMatch = descriptor[0] + '*' + descriptor[2]
        matchingFiles = filter(lambda f: fnmatch.fnmatch(f, unixMatch), os.listdir(path))
        contents = [] 
        for file in matchingFiles:
            c = openCsv(path + '/' + file)
            #print(sum([[float(getVarValue(var, file))], c[-1]], []))
            contents.append(sum([[float(getVarValue(var, file))], c[-1]], []))
        l = np.array(contents)
        #l = np.sort(np.array(contents), axis=0)
        return l

figsize=(7,3)
fig1, ax1 = plt.subplots(nrows=1, ncols=1, figsize=figsize)
fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=figsize)
fig3, ax3 = plt.subplots(nrows=1, ncols=1, figsize=figsize)

ax1.set_title("Average ratio of served needs")
ax1.set_ylabel("Percentage")
ax1.set_xlabel("Computational round interval (s)")
ax2.set_title("Average distance - arrived")
ax2.set_xlabel("Computational round interval (s)")
ax2.set_ylabel("Distance (km)")
ax3.set_title("Sum distances - wasted movement")
ax3.set_xlabel("Computational round interval (s)")
ax3.set_ylabel("Distance (km)")

for f in files:

    mean = getVector(f, 'mean')
    #print(mean)
    std = getVector(f, 'err')

    X                   = 1/mean[:,0]
    avgRatio            = mean[:,AVGRATIO + 1]
    avgRatiostd         = std[:,AVGRATIO + 1]
    avgArrivedDist      = mean[:,AVGARRIVEDDIST + 1] / 1000
    avgArrivedDiststd   = std[:,AVGARRIVEDDIST + 1] / 1000
    distN               = mean[:,DISTN + 1] / 1000
    distNstd            = std[:,DISTN + 1] / 1000

    style = '-'
    marker = 'o'
    color = 'black'
    if f.startswith('naive'):
        style = '--'
    if f.endswith('asymm'):
        c = 'blue'  
    if f.endswith('high'):
        c = 'orange'
    if f.endswith('low'):
        c = 'green'
 
    name = f.split("_")
    name = mapping[name[0]]['short']+'-'+mapping['_'+name[1]]['short']
    
    ax1.errorbar(X, avgRatio, yerr=avgRatiostd, color=c, linestyle=style, marker=marker, label=name, linewidth=1)
    #ax1.plot(X, avgRatio, color=c, linestyle=style, linewidth=3)
    #ax1.plot(X, avgRatio + avgRatiostd, color=c, linestyle=style, linewidth=0.5)
    #ax1.plot(X, avgRatio - avgRatiostd, color=c, linestyle=style, linewidth=0.5)
    
    ax2.errorbar(X, avgArrivedDist, avgArrivedDiststd, color=c, linestyle=style, marker=marker, label=name, linewidth=1)
    #ax2.plot(X, avgArrivedDist, color=c, linestyle=style, marker=marker, label=name, linewidth=3)
    #ax2.plot(X, avgArrivedDist + avgArrivedDiststd, color=c, linestyle=style, linewidth=0.5)
    #ax2.plot(X, avgArrivedDist - avgArrivedDiststd, color=c, linestyle=style, linewidth=0.5)
    
    ax3.errorbar(X, distN, distNstd, color=c, linestyle=style, marker=marker, label=name, linewidth=1)
    #ax3.plot(X, distN, color=c, linestyle=style, marker=marker, label=name, linewidth=3)
    #ax3.plot(X, distN + distNstd, color=c, linestyle=style, linewidth=0.5)
    #ax3.plot(X, distN - distNstd, color=c, linestyle=style, linewidth=0.5)

ax1.set_xscale('log')
ax1.set_ylim(ymin=0)
box = ax1.get_position()
#ax1.set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
#ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), ncol=3)
ax1.set_position([box.x0, box.y0, box.width * 0.8, box.height])
ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=1)


ax2.set_xscale('log')
ax2.set_ylim(ymin=0)
box = ax2.get_position()
ax2.set_position([box.x0, box.y0, box.width * 0.8, box.height])
ax2.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=1)

ax3.set_xscale('log')
ax3.set_ylim(ymin=0)
box = ax3.get_position()
ax3.set_position([box.x0, box.y0, box.width * 0.8, box.height])
ax3.legend(loc='center left', bbox_to_anchor=(1, 0.5), ncol=1)

fig1.tight_layout()
#fig1.show()
fig2.tight_layout()
#fig2.show()
fig3.tight_layout()
#fig3.show()


handles,labels = ax1.get_legend_handles_labels()
fig, axe= plt.subplots(nrows=1, ncols=1, figsize=(0.01,0.01))
axe.axis('off')
axe.legend(handles, labels)
fig.savefig(outputDirectory + 'res_legend_freq.pdf',bbox_inches='tight')

fig1.savefig(outputDirectory + 'res_frequency_1ratio.pdf',bbox_inches='tight')
fig2.savefig(outputDirectory + 'res_frequency_2avgdist.pdf',bbox_inches='tight')
fig3.savefig(outputDirectory + 'res_frequency_3sumdist.pdf',bbox_inches='tight')