# -*- coding: utf-8 -*-
"""
Created on Fri Apr 21 21:58:15 2017

@author: matte
"""

import os
string = '_freq-1.0.txt'
directory = 'data/newvip_raw/'
subdir = ['flexGradient/', 'naive/']
for subd in subdir:
    path = directory + subd
    #print([f.replace('.txt', string) for f in os.listdir(directory + subd) if f.endswith('.txt')])
    [os.rename(path + f, path + f.replace('.txt', string)) for f in os.listdir(directory + '/' + subd) if f.endswith('.txt')]